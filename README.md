# Treehouse Emoji

**This repo is unstable. Expect folder structure and vector image changes.**

All are welcome to use and re-purpose our emoji files as they see fit.


## Pride Set

![:achillean:](dist/pride/achillean.png?raw=true ":achillean:")
![:agender:](dist/pride/agender.png?raw=true ":agender:")
![:aromantic:](dist/pride/aromantic.png?raw=true ":aromantic:")
![:asexual:](dist/pride/asexual.png?raw=true ":asexual:")
![:blacktrans:](dist/pride/blacktrans.png?raw=true ":blacktrans:")
![:bigender:](dist/pride/bigender.png?raw=true ":bigender:")
![:bisexual:](dist/pride/bisexual.png?raw=true ":bisexual:")
![:demiboy:](dist/pride/demiboy.png?raw=true ":demiboy:")
![:demigirl:](dist/pride/demigirl.png?raw=true ":demigirl:")
![:demisexual:](dist/pride/demisexual.png?raw=true ":demisexual:")
![:genderfluid:](dist/pride/genderfluid.png?raw=true ":genderfluid:")
![:genderqueer:](dist/pride/genderqueer.png?raw=true ":genderqueer:")
![:intersex:](dist/pride/intersex.png?raw=true ":intersex:")
![:lesbian:](dist/pride/lesbian.png?raw=true ":lesbian:")
![:lesbian2:](dist/pride/lesbian2.png?raw=true ":lesbian2:")
![:nonbinary:](dist/pride/nonbinary.png?raw=true ":nonbinary:")
![:omnisexual:](dist/pride/omnisexual.png?raw=true ":omnisexual:")
![:pansexual:](dist/pride/pansexual.png?raw=true ":pansexual:")
![:polyamy:](dist/pride/polyam.png?raw=true ":polyam:")
![:polyamy2:](dist/pride/polyam2.png?raw=true ":polyam2:")
![:prorgress:](dist/pride/progress.png?raw=true ":progress:")
![:prorgress2:](dist/pride/progress2.png?raw=true ":progress2:")
![:rainbow6:](dist/pride/rainbow6.png?raw=true ":rainbow6:")
![:rainbow8:](dist/pride/rainbow8.png?raw=true ":rainbow8:")
![:rainbow9:](dist/pride/rainbow9.png?raw=true ":rainbow9:")
![:trans:](dist/pride/trans.png?raw=true ":trans:")
![:transfemme:](dist/pride/transfemme.png?raw=true ":transfemme:")
![:transmasc:](dist/pride/transmasc.png?raw=true ":transmasc:")


## Licenses

All images are published through the CC0 1.0 Universal (CC0 1.0) Public Domain Dedication.

All other files are under The 3-Clause BSD License.


## Attribution

Demiboy and Demigirl flags based on transrants designs (since taken down).

Demisexual Flag based on @AnonMoos@wikimedia.org's SVG (CC0 1.0).

Intersex Flag is based on Morgan Carpenter's [SVG design](https://ihra.org.au/22773/an-intersex-flag/) (CC0 1.0).

New Polyamory Flag courtesy of [PolyAmProud](https://polyamproud.com/flag) (CC0).

Old Polyamory Flag design (`polyam2`) is based on Jim Evan's SVG (CC0 1.0).

Omnisexual flag based on Pastelmemer's design (since taken down).

Progress flags are based on @Nikki@wikimedia.org's [Intersex Progress Flag SVG](https://commons.wikimedia.org/wiki/File:Intersex-inclusive_pride_flag.svg) (CC0 1.0).

Transgender Flag is based on Dlloyd's SVG (Public Domain).
