#!/bin/bash

LENGTH=128
MARGIN=1
WIDTH=126
HEIGHT=84

TEMP=`mktemp -d -p ""`

# pride flags
for svg_path in src/pride/*.svg; do
    svg=${svg_path#"src/pride/"}
    # resize
    convert ${svg_path} \
	    -resize ${WIDTH}x${HEIGHT} \
	    $TEMP/resized-${svg%.svg}.png
    # round borders
    magick $TEMP/resized-${svg%.svg}.png \
     \( +clone  -alpha extract \
        -draw 'fill black polygon 0,0 0,10 10,0 fill white circle 10,10 10,0' \
        \( +clone -flip \) -compose Multiply -composite \
        \( +clone -flop \) -compose Multiply -composite \
     \) -alpha off -compose CopyOpacity -composite $TEMP/rounded-${svg%.svg}.png
    # add margin
    convert $TEMP/rounded-${svg%.svg}.png \
	    -bordercolor transparent \
	    -border ${MARGIN} \
	    $TEMP/margin-${svg%.svg}.png
    # make square
    convert -background transparent \
	    -gravity center \
	    $TEMP/margin-${svg%.svg}.png \
	    -extent ${LENGTH}x${LENGTH} \
	    ./dist/pride/${svg%.svg}.png
   echo ${svg%.svg} created
done

rm -rf $TEMP

# fix and remove metadata
exiftool -all= -overwrite_original ./dist/pride/*

# create tarball for tootctl
tar -C dist/pride/ -czf dist/pride.tar.gz .
# import emojis with:
# <mastodon-dir>/bin/tootctl emoji import \
#     --category "Pride" <emoji-dir>/dist/pride.tar.gz
# or import each PNG via webadmin interface
